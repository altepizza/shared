import os
import paho.mqtt.subscribe as subscribe
from influxdb import InfluxDBClient

host = 'influxdb'
port = 8086
user = os.getenv('INFLUXDB_ADMIN_USER')
password = os.getenv('INFLUXDB_ADMIN_PASSWORD')
dbname = 'db0'
topics = [
    'desk/sensor/office_temperature/state',
    'desk/sensor/office_humidity/state',
    'bath/sensor/bath_temperature/state',
    'bath/sensor/bath_humidity/state',
    'kitchen/sensor/kitchen_temperature/state',
    'kitchen/sensor/kitchen_humidity/state',
    'livingroom/sensor/livingroom_temperature/state',
    'livingroom/sensor/livingroom_humidity/state',
    'dining/sensor/dining_temperature',
    'dining/sensor/dining_humidity/state'
]


def on_message_print(client, userdata, message):
    json_body = [
        {
            "measurement": "air",
            "fields": {
                message.topic: float(message.payload),
            }
        }
    ]
    client = InfluxDBClient(host, port, user, password, dbname)
    client.write_points(json_body)
    print(json_body)


subscribe.callback(on_message_print, topics, hostname="host.local", auth={
                   'username': 'admin', 'password': 'admin'}, port=1883)
